#!/bin/bash


# Point central
X=1000
Y=1000

# Parametre du carre
size=5

# Regex pour choisir les images
pattern=DSC_*.JPG

# prefixe et suffixe à ajouter sur les nouvelles images
prefix="WB-"
suffix=".jpg"


refR=255
refG=255
refB=255


############################################################
##                     NE PAS TOUCHER                     ##
############################################################

nb=$(ls $pattern | wc -l)

echo "Files processed :"
echo " -Nb of files : "$nb
echo " -Input       : "$pattern
echo " -Output      : "$prefix$pattern$suffix

echo ""
echo "Drawing a square :"

Xa=$(($X-$size))
Ya=$(($Y-$size))
Xb=$(($X+$size))
Yb=$(($Y+$size))

dX=$(($Xb-$Xa +1))
dY=$(($Yb-$Ya +1))
nbPxl=$(($dX * $dY))

echo " -Center    : ("$X","$Y")"
echo " -Corners   : ("$Xa","$Ya") ("$Xb","$Yb")"
echo " -Size      : "$size
echo " -Nb of Pxl : "$nbPxl

echo ""
echo "Target Color :"
echo " - ("$refR", "$refG", "$refB")"
echo ""
echo "Processed :"

cptFile=0
for f in $(ls $pattern) ;
do 
	fileIN=$f
	fileOUT=$prefix$f$suffix

	sumR=0
	sumV=0
	sumB=0
	cpt=0

	echo $cptFile"/"$nb" File processing      : "$f

	echo $cptFile"/"$nb" Recovery of pixels"

	for x in $(seq $Xa $Xb)
	do
		for y in $(seq $Ya $Yb)
		do
			coords=$x','$y
			
			ur=$(convert $fileIN -format "%[fx:255*u.p{$coords}.r]" info:)
			ug=$(convert $fileIN -format "%[fx:255*u.p{$coords}.g]" info:)
			ub=$(convert $fileIN -format "%[fx:255*u.p{$coords}.b]" info:)
			
			sumR=$(($sumR + $ur))
			sumV=$(($sumV + $ug))
			sumB=$(($sumB + $ub))
			cpt=$(($cpt + 1))
		done
	done


	valR=$(($sumR / $cpt))
	valV=$(($sumV / $cpt))
	valB=$(($sumB / $cpt))

	echo $cptFile"/"$nb" Average color found  : ("$valR", "$valV", "$valB")"


	# compute color ratio and trap for divide by zero
	if [ $valR -eq 0 ]
		then 
		redfrac=255
	else 
		redfrac=`echo "scale=3; $refR / $valR" | bc`
	fi
	if [ $valV -eq 0 ]
		then 
		greenfrac=255
	else 
		greenfrac=`echo "scale=3; $refG / $valV" | bc`
	fi
	if [ $valB -eq 0 ]
		then 
		bluefrac=255
	else 
		bluefrac=`echo "scale=3; $refB / $valB" | bc`
	fi

	# get im version
	im_version=`convert -list configure | sed '/^LIB_VERSION_NUMBER */!d;  s//,/;  s/,/,0/g;  s/,0*\([0-9][0-9]\)/\1/g' | head -n 1`

	# set up -recolor or -color-matrix
	if [ "$im_version" -lt "06060100" ]; then
		process="-recolor"
	else
		process="-color-matrix"
	fi

	# transform image color
	convert $fileIN $process "$redfrac 0 0 0 $greenfrac 0 0 0 $bluefrac" "$fileOUT"


	echo $cptFile"/"$nb" Creation of the file :" $prefix$f$suffix

	cptFile=$(($cptFile + 1))

done

echo "Done !"
