#!/bin/bash


# Point central
X=1000
Y=1000

# Parametre du carre
size=100
color="red"
width=5

# Regex pour choisir les images
pattern=DSC_*.JPG

# prefixe et suffixe � ajouter sur les nouvelles images
prefix="square-"
suffix=".jpg"



############################################################
##                     NE PAS TOUCHER                     ##
############################################################

nb=$(ls $pattern | wc -l)

echo "Files processed :"
echo " -Nb of files : "$nb
echo " -Input  : "$pattern
echo " -Output : "$prefix$pattern$suffix

echo ""
echo "Drawing a square :"

Xa=$(($X-$size))
Ya=$(($Y-$size))
Xb=$(($X+$size))
Yb=$(($Y+$size))

echo " -Center  : ("$X","$Y")"
echo " -Corners : ("$Xa","$Ya") ("$Xb","$Yb")"
echo " -Size    : "$size
echo " -Color   : "$color


cpt=0
echo ""
echo "Processed :"

for f in $(ls $pattern) ;
do 
	echo $cpt"/"$nb" : "$f" -> "$prefix$f$suffix
	convert $f -stroke $color -fill none -strokewidth $width -draw 'rectangle '$Xa','$Ya' '$Xb','$Yb $prefix$f$suffix
	cpt=$(($cpt + 1))
done

echo "Done !"
