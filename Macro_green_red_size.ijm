
function action(input, output, filename) {
	open(input+filename);
	selectWindow(filename);
	run("Split Channels");
	selectWindow(filename+" (blue)");
	run("8-bit");
	setAutoThreshold("Default");
	setThreshold(10, 150, "raw");
	setOption("BlackBackground", true);
	run("Convert to Mask");
	run("Fill Holes");
	run("Analyze Particles...", "size=105-Infinity show=Outlines display clear include add");

	selectWindow(filename+" (green)");
	n = roiManager("count");
	for (i = 0; i < n; i++) {
		roiManager("Select", i);
		roiManager("Measure");
	}
	selectWindow(filename+" (red)");
	n = roiManager("count");
	for (i = 0; i < n; i++) {
		roiManager("Select", i);
		roiManager("Measure");
	}

	saveAs("Results", output+filename+".csv");
}

inputPath = "insert the path"
outputPath = "insert the path"

setBatchMode(true); 
list = getFileList(inputPath);
for (i = 0; i < list.length; i++)
        action(inputPath, outputPath, list[i]);
setBatchMode(false);
