# Image analyses

The images were first white balanced.
First select the reference area with the "zone_affichage.sh" script by setting the "x" and "y" at the beginning of the script.
It will generate new images with a red square at the chosen position from all the "DSC_ *JPG" pattern files. 
Make sure the square is correctly positionned.
Then, put the same "x" and "y" values in the "zone_etalonnage.sh" script and run it.
It will generate new images again. The process can take some time.

To obtain the green/red ratios, run the "Macro_green_red_size.ijm" file in ImageJ after setting the input and ouput paths.
All the parameters can be adapted.

